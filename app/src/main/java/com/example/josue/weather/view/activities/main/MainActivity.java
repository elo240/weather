package com.example.josue.weather.view.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.josue.weather.R;
import com.example.josue.weather.di.HasComponent;
import com.example.josue.weather.di.components.CitiesListComponent;
import com.example.josue.weather.di.components.DaggerCitiesListComponent;
import com.example.josue.weather.di.modules.CitiesListModule;
import com.example.josue.weather.rest.dto.City;
import com.example.josue.weather.view.activities.BaseActivity;
import com.example.josue.weather.view.activities.detail.DetailActivity;
import com.example.josue.weather.view.fragments.main.AboutFragment;
import com.example.josue.weather.view.fragments.main.CitiesListFragment;
import com.example.josue.weather.view.fragments.main.DetailFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, HasComponent<CitiesListComponent> ,
        CitiesListFragment.OnHeadlineSelectedListener{
    static final String CURRENT_FRAGMENT_ID = "fragmentId";
    static final String CURRENT_TITLE = "title";

    private CitiesListComponent citiesListComponent;
    @Bind(R.id.drawer_layout)DrawerLayout drawer;
    DetailFragment detailFragment;
    CitiesListFragment citiesListFragment;
    AboutFragment aboutFragment;
    int currentFragmentId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        detailFragment = (DetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.details_frag);
        citiesListFragment = (CitiesListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.list_frag);
        aboutFragment = (AboutFragment) getSupportFragmentManager()
                .findFragmentById(R.id.about);
        configureViews();
        setActivityGraph();
        if (savedInstanceState == null){
            currentFragmentId = R.id.list_frag;
            hideFragment(aboutFragment);
        } else {
            currentFragmentId = savedInstanceState.getInt(CURRENT_FRAGMENT_ID);
            restoreView(savedInstanceState.getString(CURRENT_TITLE));
        }
    }

    private void restoreView(String title){
        int tempId = currentFragmentId;
        switch (currentFragmentId) {
            case R.id.list_frag:
                currentFragmentId = R.id.about;
                hidefragment();
                break;
            case R.id.about:
                currentFragmentId = R.id.list_frag;
                hidefragment();
                break;
            default:
                currentFragmentId = R.id.about;
                tempId = R.id.list_frag;
                hidefragment();
        }

        currentFragmentId = tempId;
        showFragment();
        setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            showDialog(R.string.title_alert_settings, R.string.text_alert_settings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt(CURRENT_FRAGMENT_ID, currentFragmentId);
        savedInstanceState.putString(CURRENT_TITLE, getTitle().toString());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        item.setChecked(true);
        String title = item.getTitle().toString();
        switch (item.getItemId()) {
            case R.id.nav_cities:
                selectItemCities(title);
                break;
            case R.id.nav_about:
                selectItemAbout(title);
                break;
            default:
                selectItemCities(title);
        }

        drawer.closeDrawer(GravityCompat.START);
        showFragment();
        return true;
    }

    @Override
    protected void setActivityGraph() {

        citiesListComponent = DaggerCitiesListComponent.builder()
                .applicationComponent(getApplicationComponent())
                .citiesListModule(new CitiesListModule())
                .build();

    }

    @Override
    public CitiesListComponent getComponent(){
        return  citiesListComponent;
    }

    @Override
    protected void configureViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        if (navigationView != null && drawer != null){
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    public void onArticleSelected(City city) {

        getApplicationComponent().getCityProvider().setCity(city);
        if (detailFragment == null) {

            Intent intent = new Intent(this, DetailActivity.class);
            startActivity(intent);

        } else {

            detailFragment.updateContent(city);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        City city = getApplicationComponent().getCityProvider().getCity();
        if (city != null){
            onArticleSelected(city);
        }
    }

    private void selectItemCities(String title) {
        if (currentFragmentId != R.id.list_frag){
            hidefragment();
        }
        currentFragmentId = R.id.list_frag;
        drawer.closeDrawers();
        setTitle(title);
    }

    private void selectItemAbout(String title) {
        if (currentFragmentId != R.id.about){
            hidefragment();
        }
        currentFragmentId = R.id.about;
        drawer.closeDrawers();
        setTitle(title);
    }

    private void showFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        switch (currentFragmentId){
            case R.id.list_frag:
                ft.show(citiesListFragment);
                if (detailFragment != null) {
                    ft.show(detailFragment);
                }
                break;
            case R.id.about:
                ft.show(aboutFragment);
                break;
        }
        ft.commit();

    }

    private void hidefragment(){
        switch (currentFragmentId){
            case R.id.list_frag:
                hideFragment(citiesListFragment);
                if (detailFragment != null){
                    hideFragment(detailFragment);
                }
                break;
            case R.id.about:
                hideFragment(aboutFragment);
                break;
        }
    }

    private void hideFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.hide(fragment);
        ft.commit();
    }
}
