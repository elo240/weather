package com.example.josue.weather.view.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.example.josue.weather.R;
import com.example.josue.weather.WeatherAplication;
import com.example.josue.weather.di.components.ApplicationComponent;


public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Each time an Activity gets created, the dependencies injection extension will be called
     */
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        this.getApplicationComponent().inject(this);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((WeatherAplication) getApplication()).getApplicationComponent();
    }

    protected void addFragment(int containerViewId, Fragment fragment) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }

    protected void showDialog(int pTitle, int pMessage) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(pTitle);
        builder.setMessage(pMessage);
        builder.setPositiveButton(getText(R.string.ok), null);
        builder.show();
    }


    /**
     * Method used to inject dependencies needed by the extending Activity
     */
    protected abstract void setActivityGraph();

    /**
     * Method used to configure views such as setting listeners
     * NOTE: (Initialization is done via ButterKnife)
     */
    protected abstract void configureViews();
}
