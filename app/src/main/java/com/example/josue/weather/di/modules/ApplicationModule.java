package com.example.josue.weather.di.modules;

import android.app.Application;
import android.content.Context;

import com.example.josue.weather.rest.dto.CityProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext(){
        return application;
    }

    @Provides
    @Singleton
    public CityProvider cityProvider(){
        return new CityProvider();
    }


}

