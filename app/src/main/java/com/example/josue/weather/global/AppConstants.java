package com.example.josue.weather.global;

/**
 * Created by josue on 12/11/15
 */
public class AppConstants {
    public static final String URL = "http://api.openweathermap.org/data/2.5/";
    public static final double LAT = 55.5;
    public static final double LON = 37.5;
    public static final int CONT = 20;
    public static final String APP_ID = "9e16b4e864396c61f82de60b9c3803fd";
    public static final String UNIT = "metric";
    public static final String UNIT_CHARACTER = " \u2103";
}
