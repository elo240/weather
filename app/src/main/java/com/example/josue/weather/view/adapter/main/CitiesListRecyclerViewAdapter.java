package com.example.josue.weather.view.adapter.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.example.josue.weather.R;
import com.example.josue.weather.global.AppConstants;
import com.example.josue.weather.rest.dto.City;
import com.example.josue.weather.view.fragments.main.CitiesListFragment;

import java.util.List;

import butterknife.Bind;

public class CitiesListRecyclerViewAdapter extends RecyclerView.Adapter<CitiesListRecyclerViewAdapter.ViewHolder> {

    private List<City> mValues;
    private OnItemClickListener mListener;
    int selectedPosition = -1;
    public interface OnItemClickListener {
        void onUserItemClicked(City partnership);
    }

    public CitiesListRecyclerViewAdapter(List<City> items, OnItemClickListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(holder.mItem.getMain().getTemp() + AppConstants.UNIT_CHARACTER);
        holder.mContentView.setText(holder.mItem.getName());
        holder.line.setSelected(false);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    selectedPosition = position;
                    mListener.onUserItemClicked(holder.mItem);
                    holder.line.setSelected(true);
                    notifyDataSetChanged();
                }
            }
        });

        if (position == selectedPosition){
            holder.line.setSelected(true);
        }


        holder.imageView.setImageDrawable(TextDrawable.builder()
                .buildRound(holder.mItem.getName().substring(0, 1), holder.mItem.getColor()));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setCities(List<City> cities){
        mValues = cities;
        addMaterialColor(mValues);
        notifyDataSetChanged();
    }

    private void addMaterialColor(List<City> cities){
        final ColorGenerator generator = ColorGenerator.MATERIAL;
        for (City city: cities){
            city.setColor(generator.getRandomColor());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public City mItem;
        public final ImageView imageView;
        public final LinearLayout line;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            imageView = (ImageView) view.findViewById(R.id.circle_image);
            line = (LinearLayout) view.findViewById(R.id.line);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
