package com.example.josue.weather.presenter;

import android.content.Context;

import com.example.josue.weather.WeatherAplication;

import javax.inject.Inject;

/**
 * Created by josue on 04/04/16.
 */
public class BasePresenter {

    protected Context context;

    @Inject
    public BasePresenter(Context context){
        this.context = context;
    }
}
