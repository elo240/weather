package com.example.josue.weather.rest.services;

import com.example.josue.weather.rest.dto.OpenWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CitiesListService {
    @GET("find")
    Call<OpenWeatherResponse> getCities(@Query("lat") double latValue,
                                        @Query("lon") double lonValue,
                                        @Query("cnt") int cntValue,
                                        @Query("appid") String appIdValue,
                                        @Query("units") String unit);
}
