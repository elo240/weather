package com.example.josue.weather.di.modules;

import com.example.josue.weather.global.AppConstants;

import java.lang.annotation.Annotation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

@Module
public class RestModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(){

        return new Retrofit.Builder()
                .baseUrl(AppConstants.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
