package com.example.josue.weather.view.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.josue.weather.R;
import com.example.josue.weather.di.components.CitiesListComponent;
import com.example.josue.weather.presenter.main.ICitiesListPresenter;
import com.example.josue.weather.rest.dto.City;
import com.example.josue.weather.view.adapter.main.CitiesListRecyclerViewAdapter;
import com.example.josue.weather.view.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CitiesListFragment extends BaseFragment implements ICitieListFragment, SwipeRefreshLayout.OnRefreshListener {
    @Inject
    ICitiesListPresenter citiesListPresenter;
    CitiesListRecyclerViewAdapter adapter;
    @Bind(R.id.swipe_refresh_layout) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.empty_layout) LinearLayout emptyLayout;
    OnHeadlineSelectedListener mCallback;

    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        void onArticleSelected(City city);
    }


    public CitiesListFragment() {
    }

    public static CitiesListFragment newInstance() {
        return new CitiesListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_list, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.city);
        ButterKnife.bind(this, view);
        if (adapter == null) {
            Context context = view.getContext();
            adapter = new CitiesListRecyclerViewAdapter(new ArrayList<City>(), onItemClickListener);

            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.getComponent(CitiesListComponent.class).inject(this);
        this.citiesListPresenter.setCitieListFragment(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        if (adapter.getItemCount() == 0){
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    citiesListPresenter.getCities();
                }
            });
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void loadCities(List<City> cities) {
        adapter.setCities(cities);
        swipeRefreshLayout.setRefreshing(false);
        emptyLayout.setVisibility(View.GONE);
    }

    @Override
    public void showError(int pTilte, int pText) {
        showDialog(pTilte, pText);
        swipeRefreshLayout.setRefreshing(false);
        if (adapter.getItemCount() < 1){
            emptyLayout.setVisibility(View.VISIBLE);
        }


    }

    private CitiesListRecyclerViewAdapter.OnItemClickListener onItemClickListener =
            new CitiesListRecyclerViewAdapter.OnItemClickListener() {
                @Override public void onUserItemClicked(City partnership) {
                    mCallback.onArticleSelected(partnership);
                }
            };

    @Override
    public void onRefresh() {
        this.citiesListPresenter.getCities();
    }
}
