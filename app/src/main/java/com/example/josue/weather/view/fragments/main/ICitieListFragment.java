package com.example.josue.weather.view.fragments.main;

import com.example.josue.weather.rest.dto.City;

import java.util.List;

/**
 * Created by josue on 04/04/16.
 */
public interface ICitieListFragment {
    void loadCities(List<City> cities);
    void showError(int pTilte, int pText);
}
