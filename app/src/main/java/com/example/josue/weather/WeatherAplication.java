package com.example.josue.weather;

import android.app.Application;

import com.example.josue.weather.di.components.ApplicationComponent;
import com.example.josue.weather.di.components.DaggerApplicationComponent;
import com.example.josue.weather.di.modules.ApplicationModule;

public class WeatherAplication extends Application {
    private ApplicationComponent applicationComponent;
    private ApplicationModule applicationModule;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationModule = new ApplicationModule(this);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(getApplicationModule()).build();
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }

    public ApplicationModule getApplicationModule(){
        return applicationModule;
    }
}

