package com.example.josue.weather.view.activities.detail;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.josue.weather.R;
import com.example.josue.weather.rest.dto.City;
import com.example.josue.weather.view.activities.BaseActivity;
import com.example.josue.weather.view.fragments.main.DetailFragment;

public class DetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        configureViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            getApplicationComponent().getCityProvider().setCity(null);
            finish();
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setActivityGraph() {

    }

    @Override
    public void onBackPressed() {
        getApplicationComponent().getCityProvider().setCity(null);
        super.onBackPressed();
    }

    @Override
    protected void configureViews() {

        DetailFragment displayFrag = (DetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.details_frag);
        City city = getApplicationComponent().getCityProvider().getCity();
        setTitle(city.getName());
        displayFrag.updateContent(city);
    }
}
