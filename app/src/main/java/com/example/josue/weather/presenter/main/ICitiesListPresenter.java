package com.example.josue.weather.presenter.main;

import com.example.josue.weather.view.fragments.main.ICitieListFragment;

public interface ICitiesListPresenter {
    void getCities();
    void setCitieListFragment(ICitieListFragment citieListFragment);
}
