package com.example.josue.weather.di.components;

import android.content.Context;

import com.example.josue.weather.di.modules.ApplicationModule;
import com.example.josue.weather.di.modules.RestModule;
import com.example.josue.weather.rest.dto.CityProvider;
import com.example.josue.weather.view.activities.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {ApplicationModule.class, RestModule.class})
public interface ApplicationComponent {
    Context context();
    Retrofit getRetrofit();
    CityProvider getCityProvider();
    void inject(BaseActivity baseActivity);
}

