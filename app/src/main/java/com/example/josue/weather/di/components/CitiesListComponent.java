package com.example.josue.weather.di.components;

import com.example.josue.weather.di.modules.CitiesListModule;
import com.example.josue.weather.di.scopes.FragmentScope;
import com.example.josue.weather.view.fragments.main.CitiesListFragment;

import dagger.Component;

@FragmentScope
@Component(modules = {CitiesListModule.class},
        dependencies = {ApplicationComponent.class})
public interface CitiesListComponent {
    void inject(CitiesListFragment citiesListFragment);
}
