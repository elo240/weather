package com.example.josue.weather.di.modules;

import android.content.Context;

import com.example.josue.weather.di.scopes.FragmentScope;
import com.example.josue.weather.presenter.main.CitiesListPresenter;
import com.example.josue.weather.presenter.main.ICitiesListPresenter;
import com.example.josue.weather.rest.services.CitiesListService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class CitiesListModule {

    @Provides
    @FragmentScope
    public CitiesListService provideAdsService(Retrofit retrofit){
        return retrofit.create(CitiesListService.class);
    }

    @Provides
    @FragmentScope
    public ICitiesListPresenter provideIAdPresenter(Context context, CitiesListService service) {
        return new CitiesListPresenter(context, service) {
        };
    }
}
