package com.example.josue.weather.presenter.main;

import android.content.Context;

import com.example.josue.weather.R;
import com.example.josue.weather.global.AppConstants;
import com.example.josue.weather.presenter.BasePresenter;
import com.example.josue.weather.rest.dto.OpenWeatherResponse;
import com.example.josue.weather.rest.services.CitiesListService;
import com.example.josue.weather.view.fragments.main.ICitieListFragment;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by josue on 04/04/16.
 *
 */
public class CitiesListPresenter extends BasePresenter implements ICitiesListPresenter, Callback<OpenWeatherResponse> {
    CitiesListService service;
    ICitieListFragment citieListFragment;

    @Inject
    public CitiesListPresenter(Context context, CitiesListService service) {
        super(context);
        this.service = service;
    }

    @Override
    public void setCitieListFragment(ICitieListFragment citieListFragment) {
        this.citieListFragment = citieListFragment;
    }

    @Override
    public void getCities() {
        Call<OpenWeatherResponse> call = service.getCities(AppConstants.LAT,
                AppConstants.LON, AppConstants.CONT, AppConstants.APP_ID,
                AppConstants.UNIT);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<OpenWeatherResponse> response) {
        if (response.isSuccess()){
            citieListFragment.loadCities(response.body().getCity());
        }

    }

    @Override
    public void onFailure(Throwable t) {
        citieListFragment.showError(R.string.title_connection_error, R.string.text_connection_error);
    }
}
