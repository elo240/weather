package com.example.josue.weather.rest.dto;

/**
 * Created by josue on 05/04/16.
 */
public class CityProvider {
    City city;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
