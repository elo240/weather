package com.example.josue.weather.view.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.josue.weather.R;
import com.example.josue.weather.global.AppConstants;
import com.example.josue.weather.rest.dto.City;

import butterknife.Bind;
import butterknife.ButterKnife;


public class DetailFragment extends Fragment {
    String FORMAT = "%s %s";
    String PERCENT = "%";
    String SPEED = "km/h";
    @Bind(R.id.city_name) TextView cityName;
    @Bind(R.id.weather_description) TextView weatherDescription;
    @Bind(R.id.temperature) TextView temperature;
    @Bind(R.id.temperature_unit) TextView temperatureUnit;
    @Bind(R.id.humidity) TextView humidity;
    @Bind(R.id.wind_speed) TextView windSpeed;
    @Bind(R.id.card) CardView cardView;
    private City city;

    public DetailFragment() {
        // Required empty public constructor
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void updateContent(City city){
        String sHumity = String.format(FORMAT, city.getMain().getHumidity(), PERCENT);
        String sWind = String.format(FORMAT, city.getWind().getSpeed(), SPEED);
        cityName.setText(city.getName());
        temperature.setText(city.getMain().getTemp());
        temperatureUnit.setText(AppConstants.UNIT_CHARACTER);
        humidity.setText(sHumity);
        windSpeed.setText(sWind);

        String description = "";
        if (city.getWeather().size() > 0){
            description += city.getWeather().get(0).getDescription();
        }

        weatherDescription.setText(description);
        cardView.setVisibility(View.VISIBLE);
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, view);
        if (city != null){
            updateContent(city);
        } else {
            cardView.setVisibility(View.GONE);
        }
        return view;
    }
}
